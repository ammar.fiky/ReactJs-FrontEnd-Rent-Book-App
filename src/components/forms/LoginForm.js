import React, { Component } from "react"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Paper from "@material-ui/core/Paper"
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import { Link, Redirect } from "react-router-dom"
import Typography from "@material-ui/core/Typography"
import Hidden from "@material-ui/core/Hidden"
import logo from "../../assets/bookshelf.svg"
import jwt from "../../helpers/jwt"
import Validator from "validator"
import PropTypes from "prop-types"
import InlineError from "../messages/InlineError.js"
// import axios from "axios"

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" to="https://github.com/tejojr">
        ZerefWeismann
      </Link>{" "}
      {new Date().getFullYear()}
      {". Built with "}
      <Link color="inherit" to="https://material-ui.com/">
        Material-UI.
      </Link>
    </Typography>
  )
}
const styles = {
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/1600x900/?book)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: "80px 32px",
    display: "flex",
    flexDirection: "column"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: "8px"
  },
  submit: {
    margin: "24px 0px 16px"
  },
  title: {
    color: "#FFFFFF",
    marginLeft: "80px",
    marginTop: "80px"
  },
  rightImage: {
    float: "right",
    marginRight: "32px",
    marginTop: "8px"
  },
  setLink: {
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline"
    }
  }
}

class LoginForm extends Component {
  state = {
    data: {
      email: "",
      password: ""
    },
    loading: false,
    errors: {}
  }

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    })
  onSubmit = e => {
    e.preventDefault()
    const errors = this.validate(this.state.data)
    this.setState({ errors })
    if (Object.keys(errors).length === 0) {
      this.props.submit(this.state.data)
    }
  }
  validate = data => {
    const errors = {}
    if (!data.email) errors.email = "Please fill the email box"
    if (!data.password) errors.password = "please fill the password box"
    return errors
  }
  handleLogin(e) {
    e.preventDefault()
    jwt.logIn(this.state.data).then(user => {
      this.setState({ data: { email: "", password: "" } })
      if (user) {
        this.setState({ toHome: true })
      }
    })
  }
  render() {
    const { data, errors } = this.state
    return (
      <Grid container component="main" style={styles.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} style={styles.image}>
          <Hidden smDown>
            <Typography component="h1" variant="h4" style={styles.title}>
              Book is a window to the World ...
            </Typography>
          </Hidden>
        </Grid>
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Link to="/">
            <img src={logo} alt="logo" style={styles.rightImage} />
          </Link>
          <div style={styles.paper}>
            <Typography component="h3" variant="h3">
              Sign in
            </Typography>
            <Typography component="h1" variant="h6">
              Welcome Back, Please Login to your account
            </Typography>
            <form style={styles.form} noValidate>
              <TextField
                error={!!errors.email}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address or Username"
                name="email"
                autoComplete="email"
                onChange={this.onChange}
                value={data.email}
                autoFocus
              />
              {errors.email && <InlineError text={errors.email} />}

              <TextField
                error={!!errors.password}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={this.onChange}
                value={data.password}
              />
              {errors.password && <InlineError text={errors.password} />}
              <br></br>
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                onClick={this.onSubmit}
                fullWidth
                variant="contained"
                color="primary"
                style={styles.submit}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link to="/register" variant="body2" style={styles.setLink}>
                    Forgot Password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="/register" variant="body2" style={styles.setLink}>
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}>
                <Copyright />
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
    )
  }
}
LoginForm.propTypes = {
  submit: PropTypes.func.isRequired
}

export default LoginForm
